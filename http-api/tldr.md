# TL;DR

## Principles

The guidelines are driven by a set of principles and assumes teams adhere to the
same.

| Principle 1   | Principle 2   | Principle 3   |
| ------------- | ------------- | ------------- |
| Description 1 | Description 2 | Description 3 |

_Full version [here](http://google.com)_

## Naming Conventions

| Item                | Convention        |
| ------------------- | ----------------- |
| URI path components | `kebab-case`      |
| Query params        | `camelCase`       |
| JSON response       | `camelCase`       |
| Enum value          | `WITH_UNDERSCORE` |

**You should do this**

- :white_check_mark: `https://app.com/products?brandName=foo`
- :white_check_mark: `https://app.com/invoice-items`
- :white_check_mark: `{ "products": { "salePrice": 1.99 } }`
- :white_check_mark: `{ "products": { "status": "SOLD_OUT" } }`

**You should not do this**

- :x: `https://app.com/Products`
- :x: `https://app.com/Products?brand_name=foo`
- :x: `https://app.com/Products?brand-name=foo`
- :x: `https://app.com/invoiceItems`

_Full version [here](http://google.com)_

## Status Codes

| Range                      | Meaning                       |
| -------------------------- | ----------------------------- |
| :white_check_mark: **2xx** | Successfull execution         |
| :bomb: **4xx**             | Client sent _bad_ information |
| :boom: **5xx**             | Unexpected server error       |

See [here](http://google.com) how to use the ranges.

:cherries: **Important to know:**

- All **4xx** and **5xx** must return the same error representation.
- All **4xx** should return enough information to the client describing the
  cause of the error and how to fix it.
- :warning: Be careful to not expose sensitive information with the errors.
