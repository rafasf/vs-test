---
sidebarDepth: 2
---

# Hello

First stop when looking for guidelines.

## Diagrams

```mermaid
sequenceDiagram
Alice->John: Hello John, how are you?
loop every minute
    John-->Alice: Great!
end
```
