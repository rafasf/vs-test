module.exports = {
  title: "Engineering Cheatsheet",
  description: "Cheatsheet tagline",
  dest: "public",
  plugins: ["mermaidjs"],
  themeConfig: {
    sidebar: [
      "/",
      {
        title: "Some decisions",
        path: "/decisions",
        children: ["/decisions/0001-record-architecture-decisions"],
      },
      {
        title: "HTTP API",
        path: "/http-api/",
        collapsable: false,
        children: ["/http-api/tldr"],
      },
    ],
  },
};
